﻿using EAP_PracticeExam_Client.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace EAP_PracticeExam_Client.Controllers
{
    public class EmployeeController : Controller
    {
        private string hostUrl = "http://localhost:51050/Service1.svc/rest/";
        public async System.Threading.Tasks.Task<ActionResult> IndexAsync()
        {
            List<Employee> employees = new List<Employee>();
            using (var client = new HttpClient())
            {
                // chuyen doi url
                client.BaseAddress= new Uri(hostUrl);
                client.DefaultRequestHeaders.Clear();

                //
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                //
                HttpResponseMessage httpResponse = await client.GetAsync("getemployees/");

                //
                if (httpResponse.IsSuccessStatusCode)
                {
                    var empResp = httpResponse.Content.ReadAsStringAsync().Result;

                    //
                    employees = JsonConvert.DeserializeObject<List<Employee>>(empResp);
                }
                return View(employees);


            }
        }
    }
}