﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EAP_PracticeExam
{
    [DataContract]
    public class Employee
    {
        [DataMember]

        public int id { get; set; }
        [DataMember]

        public string name { get; set; }
        [DataMember]

        public double salaty { get; set; }
        [DataMember]

        public string department { get; set; }
        [DataMember]

        public string status { get; set; }

        public interface IEmployeeResponse
        {
            List<Employee> GetEmployees();
            Employee AddEmployee(Employee employee);
            Employee SearchEmployee(string name);

        }

        public class EmployeeResponse : IEmployeeResponse
        {
            private List<Employee> employees = new List<Employee>();
            private List<Employee> employeesByname = new List<Employee>();

            private int couter = 1;
            public EmployeeResponse()
            {
                AddEmployee(new Employee { name = "Nguyen Van A", salaty = 3000000, department = "TEST", status = "enable" });
                AddEmployee(new Employee { name = "Nguyen Van B", salaty = 2500000, department = "TEST", status = "enable" });
                AddEmployee(new Employee { name = "Nguyen Van C", salaty = 3500000, department = "TEST", status = "enable" });
                AddEmployee(new Employee { name = "Nguyen Van D", salaty = 4000000, department = "TEST", status = "enable" });
            }
            //add employee
            public Employee AddEmployee(Employee employee)
            {
                if (employee == null) throw new ArgumentException("new employee");

                employee.id = couter++;
                employees.Add(employee);
                return employee;
            }



            public List<Employee> GetEmployees()
            {
                return employees;
            }

            public Employee SearchEmployee(string name)
            {
                Employee employee = employees.Find(b => b.name == name);
                if (employee == null)
                {
                    throw new ArgumentNullException("not found");
                }
                return employee;
            }
        }
    }
}
