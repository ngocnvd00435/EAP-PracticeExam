﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using static EAP_PracticeExam.Employee;

namespace EAP_PracticeExam
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        static IEmployeeResponse response = new EmployeeResponse();

        public Employee AddEmployee(Employee employee)
        {
            return response.AddEmployee(employee);
        }


        public void DoWork()
        {
        }


        public List<Employee> GetEmployees()
        {
            return response.GetEmployees();
        }

        public Employee SearchEmployee(string name)
        {
            return response.SearchEmployee(name);
        }
    }
}
